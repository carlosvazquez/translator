<?php
class Translator {
   private $_Wtranslator = array();
    
   public function __construct($lenguage = null) {
       if(!$lenguage){
         $this->_Wtranslator = $this->_es();
       }else{
           $this->_Wtranslator = $this->$lenguage();
       }
   }
   
   public function _getTranslation($key){       
       if(isset($this->_Wtranslator[$key])){
           return $this->_Wtranslator[$key];
       }
   }
   
   public function _es(){
       return array(
           'Bienvendo'=>'Bienvenido'
       );
   }
   
   public function _en(){
       return array(
           'Bienvendo'=>'Welcome'
       );
   }
}